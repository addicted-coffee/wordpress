
module.exports = grunt => {
	// Load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
	require('load-grunt-tasks')(grunt);

  const sass = require('node-sass');

	grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      sass: {
        files: [
          'src/scss/**/*.scss',
        ],
        tasks: ['sass'],
        options: {
          livereload: true,
        },
      },
      js: {
        files: [
          'src/js/**/*.js',
        ],
        tasks: ['concat'],
        options: {
          livereload: true,
        },
      },
      images: {
        files: ['src/img/**/*.{png,jpg,gif}'],
        tasks: ['imagemin'],
        options: {
          livereload: true,
        },
      },
    },

    sass: {
      options: {
        implementation: sass,
        sourceMap: true
      },
      dist: {
        files: {
          'dist/css/application.css': 'src/scss/application.scss'
        }
      }
    },

    concat: {
      options: {
        separator: '',
        sourceMap: true,
      },
      js: {
        src: [
          'node_modules/jquery/dist/jquery.min.js',
          'node_modules/popper.js/dist/umd/popper.min.js',
          'node_modules/bootstrap/dist/js/bootstrap.min.js',
          'node_modules/slick-carousel/slick/slick.min.js',
          'src/js/main.js',
        ],
        dest: 'dist/js/production.js',
      },
    },

    uglify: {
      prod: {
        src: [
          'dist/js/production.js',
        ],
        dest: 'dist/js/production.min.js'
      },
    },

    cssmin: {
      css:{
        src: 'dist/css/application.css',
        dest: 'dist/css/application.min.css',
      }
    },

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'src/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'dist/'
        }],
      },
    },
  });

  grunt.registerTask('default', ['concat', 'sass', 'imagemin', 'watch']);
  grunt.registerTask('prod', ['concat', 'uglify', 'sass', 'cssmin', 'imagemin']);
};
