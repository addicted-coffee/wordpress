<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		<?php if (WP_ENV == "development"): ?>
			<script src="http://localhost:35729/livereload.js"></script>
			<meta http-equiv="cache-control" content="no-cache, must-revalidate" />
			<meta http-equiv="pragma" content="no-cache" />
		<?php endif; ?>

		<?php wp_head(); ?>
  </head>
<body <?php body_class();?>>
  <?php get_template_part('templates/content', 'navbar'); ?>
